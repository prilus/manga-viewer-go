module gitlab.com/prilus/manga-viewer-go

go 1.16

require (
	github.com/facette/natsort v0.0.0-20181210072756-2cd4dd1e2dcb // indirect
	github.com/mattn/go-sqlite3 v1.14.16
	github.com/satori/go.uuid v1.2.0
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
