package main

import (
	"embed"
	"flag"
	"fmt"
	"io/fs"
	"log"
	"net/http"
	"os"
	"reflect"
	"strings"

	_ "gitlab.com/prilus/manga-viewer-go/source/db"

	"gitlab.com/prilus/manga-viewer-go/source/dir"
	"gitlab.com/prilus/manga-viewer-go/source/mangaviewer"
	"gitlab.com/prilus/manga-viewer-go/source/session"
	"gitlab.com/prilus/manga-viewer-go/source/users"
)

const (
	handlePrefix = "/service/"
)

//go:embed static
var staticFiles embed.FS

var serverBind string
var serverPort int

var handleMap map[string]reflect.Value
var handleList struct {
	Dir         dir.Dir
	MangaViewer mangaviewer.MangaViewer
	Users       users.Users
	Admin       users.Admin
}

func HandleFunc(w http.ResponseWriter, r *http.Request) {
	//fmt.Println(r.URL.Path)

	if f, ok := handleMap[r.URL.Path]; ok {
		s := reflect.ValueOf(session.NewRequest(w, r))
		f.Call([]reflect.Value{s})
	} else {
		w.WriteHeader(404)
		w.Write([]byte("404 not found"))
	}
}

func runServer(bindaddr string, port int) {
	handle := http.NewServeMux()

	handle.HandleFunc(handlePrefix, HandleFunc)

	var staticFS = fs.FS(staticFiles)
	htmlContent, err := fs.Sub(staticFS, "static")
	if err != nil {
		log.Fatal(err)
	}

	handle.Handle("/", http.FileServer(http.FS(htmlContent)))

	if err := http.ListenAndServe(fmt.Sprintf("%s:%d", bindaddr, port), handle); err != nil {
		log.Println(err)
	}
}

func initHandle() {
	handleMap = make(map[string]reflect.Value)

	v := reflect.ValueOf(handleList)

	for i := 0; i < v.NumField(); i++ {
		prefix := handlePrefix + v.Field(i).Type().Name() + "/"

		vv := v.Field(i)
		for j := 0; j < vv.NumMethod(); j++ {
			key := strings.ToLower(prefix + vv.Type().Method(j).Name)
			handleMap[key] = vv.Method(j)

			fmt.Printf("handle add %s\n", key)
		}
	}
}

func init() {
	pserverBind := flag.String("bind", "0.0.0.0", "server bind address")
	pserverPort := flag.Int("port", 5000, "Server Port")

	flag.Parse()

	if flag.NFlag() == 0 && false {
		flag.Usage()
		os.Exit(-1)
	}

	serverBind = *pserverBind
	serverPort = *pserverPort

	initHandle()

	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
}

func main() {
	runServer(serverBind, serverPort)
}
