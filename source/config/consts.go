package config

const (
	COMICS_PREFIX = "comics/"

	SESSION_COOKIENAME = "__sessionKey"
	SESSION_DIR = "session_dir/"

	LOG_DIR = "log/"

	DATABASE_FILENAME = "user.db"

	TABLENAME_USER = "user"

	ID_REGEX = "^[0-9a-zA-Z\\-_\\.]{1,32}$"
)