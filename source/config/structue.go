package config

type APIResult struct {
	Result string
	Data interface{}
}