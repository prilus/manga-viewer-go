package users

import (
	"encoding/json"
	"regexp"

	"gitlab.com/prilus/manga-viewer-go/source/config"
	"gitlab.com/prilus/manga-viewer-go/source/session"
)

type Users struct{}

func (Users) Login(s *session.Request) {
	status := "ok"
	Id := s.Param("id")
	Pass := s.Param("pass")
	Level := -1

	defer func() {
		b, _ := json.Marshal(config.APIResult{Result: status, Data: Level})
		s.ResponseData(b)
	}()

	if len(Id) == 0 || len(Pass) == 0 {
		status = "^^"
		return
	}

	user := GetUser(Id)

	if user == nil {
		status = "id non exist"
		return
	} else if !user.CheckPass(Pass) {
		status = "password diff"
		return
	}

	s.LoadSession()
	s.S.SetData("Id", Id)
	Level = int(user.Level)
}

func (Users) Register(s *session.Request) {
	status := "ok"
	Id := s.Param("id")
	Pass := s.Param("pass")

	defer func() {
		b, _ := json.Marshal(config.APIResult{Result: status})
		s.ResponseData(b)
	}()

	if len(Id) == 0 || len(Pass) == 0 {
		status = "^^"
		return
	}

	if p := regexp.MustCompile(config.ID_REGEX); !p.MatchString(Id) {
		status = "Id can contain up to 32 alphanumeric characters."
		return
	}

	if register(Id, Pass) {
		return
	} else {
		status = "already id exist"
		return
	}
}

func (Users) Logout(s *session.Request) {
	status := "ok"

	s.LoadSession()
	s.S.SetData("Id", "")

	b, _ := json.Marshal(config.APIResult{Result: status})
	s.ResponseData(b)
}

// TODO: req, res 구조체 함수 인자, 반환 값으로 변경하고 front code generator 만들기
type _UserInfoRes struct {
	Id    string
	Level int
}

func (Users) Info(s *session.Request) {
	level := GetLoginIdLevel(s)
	id := s.S.GetData("Id")

	if id == "" {
		b, _ := json.Marshal(config.APIResult{Result: "no login", Data: "{}"})
		s.ResponseData(b)
		return
	}

	d := _UserInfoRes{id, level}
	b, _ := json.Marshal(config.APIResult{Result: "ok", Data: &d})

	s.ResponseData(b)
}
