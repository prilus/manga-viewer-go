package users

import (
	"fmt"
	"log"
	"sync"

	"gitlab.com/prilus/manga-viewer-go/source/config"
	"gitlab.com/prilus/manga-viewer-go/source/db"
	"gitlab.com/prilus/manga-viewer-go/source/session"
	"gitlab.com/prilus/manga-viewer-go/source/util"
)

type UserInfo struct {
	Id    string
	Pass  string `json:"-"`
	Level int32
}

var UserList map[string]*UserInfo
var UserMutex sync.RWMutex

func init() {
	UserList = make(map[string]*UserInfo)

	reload()
}

func reload() {
	UserMutex.RLock()
	defer UserMutex.RUnlock()

	var user []*UserInfo

	query := fmt.Sprintf("select %s from %s", util.GetAllField(&UserInfo{}), config.TABLENAME_USER)
	db.Query(&user, query)

	for _, a := range user {
		b := a
		UserList[a.Id] = b
	}

	log.Println("loaded userlist")
}

func register(Id string, Pass string) bool {
	UserMutex.RLock()
	if _, ok := UserList[Id]; ok {
		UserMutex.RUnlock()
		return false
	}
	UserMutex.RUnlock()

	query := fmt.Sprintf("insert into %s (%s) values (%s)", config.TABLENAME_USER, util.GetAllField(&UserInfo{}), util.GetAllQuestionMark(&UserInfo{}))
	_, err := db.Exec(query, Id, util.Hash(Pass), 1)

	if err == nil {
		UserMutex.Lock()
		UserList[Id] = &UserInfo{Id, util.Hash(Pass), 1}
		UserMutex.Unlock()
	}

	return err == nil
}

func list() []*UserInfo {
	UserMutex.RLock()
	defer UserMutex.RUnlock()

	var list = make([]*UserInfo, 0, len(UserList))

	for _, v := range UserList {
		list = append(list, v)
	}

	return list
}

func GetLoginIdLevel(s *session.Request) int {
	s.LoadSession()
	id := s.S.GetData("Id")

	if len(id) > 0 {
		UserMutex.RLock()
		defer UserMutex.RUnlock()

		if user, ok := UserList[id]; ok {
			return int(user.Level)
		}

		s.S.SetData("Id", "")
	}

	return -1
}

func GetUser(Id string) *UserInfo {
	UserMutex.RLock()
	defer UserMutex.RUnlock()

	if user, ok := UserList[Id]; ok {
		return user
	}

	return nil
}

func (t *UserInfo) CheckPass(Pass string) bool {
	UserMutex.RLock()
	defer UserMutex.RUnlock()

	if util.Hash(Pass) == t.Pass {
		return true
	}

	_ = t.Pass

	// 구버전 호환성
	//if util.HashMD5(Pass) == this.Pass || util.HashMysql51(Pass) == this.Pass {
	//	this.UpdatePass(Pass)
	//	return true
	//}

	return false
}

func (t *UserInfo) SetLevel(Level int) {
	UserMutex.Lock()
	defer UserMutex.Unlock()

	t.Level = int32(Level)
	query := fmt.Sprintf("update %s set Level = ? where Id = ?", config.TABLENAME_USER)
	db.Exec(query, Level, t.Id)
}

func (t *UserInfo) UpdatePass(Pass string) {
	UserMutex.Lock()
	defer UserMutex.Unlock()

	var hashpass = util.Hash(Pass)
	t.Pass = hashpass
	query := fmt.Sprintf("update %s set Pass = ? where Id = ?", config.TABLENAME_USER)
	db.Exec(query, t.Id, hashpass)
}
