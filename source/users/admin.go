package users

import (
	"encoding/json"

	"gitlab.com/prilus/manga-viewer-go/source/config"
	"gitlab.com/prilus/manga-viewer-go/source/session"
)

type Admin struct{}

func (Admin) List(s *session.Request) {
	status := "ok"
	var d interface{}

	defer func() {
		b, _ := json.Marshal(config.APIResult{Result: status, Data: d})
		s.ResponseData(b)
	}()

	if GetLoginIdLevel(s) < 100 {
		status = "permission denied"
		return
	}

	d = list()
}

func (Admin) SetLevel(s *session.Request) {
	status := "ok"
	id := s.Param("Id")
	level := s.IntParam("Level")

	defer func() {
		b, _ := json.Marshal(config.APIResult{Result: status})
		s.ResponseData(b)
	}()

	if GetLoginIdLevel(s) < 100 {
		status = "permission denied"
		return
	}
	if len(id) == 0 && level < 1 {
		status = "invaild data"
		return
	}

	user := GetUser(id)

	if user == nil {
		status = "non exist user"
		return
	}

	user.SetLevel(level)
}

func (Admin) Delete(s *session.Request) {
	_ = 1
}
