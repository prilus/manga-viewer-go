package mangaviewer

import (
	"archive/zip"
	"io/ioutil"
	"log"
	"sort"

	"github.com/facette/natsort"
	"gitlab.com/prilus/manga-viewer-go/source/config"
	"gitlab.com/prilus/manga-viewer-go/source/util"
)

type ZipHandler struct {
	zipHandle *zip.ReadCloser
}

func OpenZipFile(filename string) (handle *ZipHandler) {
	if !util.CheckHackPath(filename) {
		return nil
	}

	zipHandle, err := zip.OpenReader(config.COMICS_PREFIX + filename)
	if err != nil {
		log.Println(err)
		return
	}

	sort.Slice(zipHandle.File, func(i, j int) bool {
		return natsort.Compare(zipHandle.File[i].Name, zipHandle.File[j].Name)
	})

	handle = &ZipHandler{
		zipHandle: zipHandle,
	}

	return
}

func (t *ZipHandler) List() []string {
	list := make([]string, 0, t.Count())

	for _, v := range t.zipHandle.File {
		list = append(list, v.Name)
	}

	return list
}

func (t *ZipHandler) Close() {
	t.zipHandle.Close()
}

func (t *ZipHandler) checkIdx(Idx int) bool {
	if Idx >= 0 && Idx < t.Count() {
		return true
	}
	return false
}

func (t *ZipHandler) GetFile(Idx int) (string, []byte) {
	if t.checkIdx(Idx) {
		f := t.zipHandle.File[Idx]
		if handle, err := f.Open(); err == nil {
			defer handle.Close()

			if buf, err := ioutil.ReadAll(handle); err == nil {
				return f.Name, buf
			}
		}
	}
	return "", nil
}

func (t *ZipHandler) Count() int {
	return len(t.zipHandle.File)
}
