package mangaviewer

import (
	"encoding/json"

	"gitlab.com/prilus/manga-viewer-go/source/config"
	"gitlab.com/prilus/manga-viewer-go/source/session"
	"gitlab.com/prilus/manga-viewer-go/source/users"
	"gitlab.com/prilus/manga-viewer-go/source/util"
)

type MangaViewer struct{}

func (MangaViewer) Test(s *session.Request) {
	s.ResponseData([]byte("hello world!"))
}

func (MangaViewer) List(s *session.Request) {
	filename := s.Param("filename")
	var status = "ok"
	var list interface{}

	defer func() {
		b, _ := json.Marshal(config.APIResult{Result: status, Data: list})
		s.ResponseData(b)
	}()

	if users.GetLoginIdLevel(s) < 2 {
		status = "permission denied"
		return
	}

	if handle := OpenZipFile(filename); handle != nil {
		list = handle.List()
		handle.Close()
	} else {
		status = "file not found!"
	}
}

func (MangaViewer) Info(s *session.Request) {
	filename := s.Param("filename")
	var status = "ok"
	var n = -1

	defer func() {
		b, _ := json.Marshal(config.APIResult{Result: status, Data: n})
		s.ResponseData(b)
	}()

	if users.GetLoginIdLevel(s) < 2 {
		status = "permission denied"
		return
	}

	if handle := OpenZipFile(filename); handle != nil {
		n = handle.Count()
		handle.Close()
	} else {
		status = "file not found!"
	}
}

func (MangaViewer) GetImage(s *session.Request) {
	filename := s.Param("filename")
	page := s.IntParam("page")

	if users.GetLoginIdLevel(s) < 2 {
		s.ResponseData(util.HttpCodeImg(403))
		return
	}

	handle := OpenZipFile(filename)
	if handle == nil {
		s.ResponseData(util.HttpCodeImg(416))
		return
	}
	defer handle.Close()

	_, data := handle.GetFile(page)
	if data == nil {
		s.ResponseData(util.HttpCodeImg(416))
		return
	}

	s.SetHeader("Cache-Control", "private, max-age=3600")
	s.ResponseData(data)
}
