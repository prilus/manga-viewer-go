package db

import (
	"database/sql"
	"fmt"
	"log"
	"reflect"
	"runtime/debug"
	"strconv"
	"sync"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/prilus/manga-viewer-go/source/config"
)

var dbMutex sync.Mutex
var conn *sql.DB = nil

func init() {
	log.Println("database init " + config.DATABASE_FILENAME)

	if conn != nil {
		return
	}
	if db, err := sql.Open("sqlite3", config.DATABASE_FILENAME); err == nil {
		conn = db
	} else {
		panic(err)
	}
}

func goQuery(query string, va ...interface{}) (*sql.Rows, error) {
	dbMutex.Lock()
	defer dbMutex.Unlock()

	return conn.Query(query, va...)
}

func goExec(query string, va ...interface{}) (r sql.Result, err error) {
	dbMutex.Lock()
	defer dbMutex.Unlock()

	tx, err := conn.Begin()
	r, err = tx.Exec(query, va...)
	if err != nil {
		tx.Rollback()
		log.Println(err)
		debug.PrintStack()
		return
	}
	tx.Commit()
	return
}

func Query(dest interface{}, query string, va ...interface{}) {
	rows, err := goQuery(query, va...)
	if err != nil {
		log.Println(err)
		debug.PrintStack()
		return
	}

	col, err := rows.Columns()
	if err != nil {
		log.Println(err)
		return
	}

	defer rows.Close()

	v := reflect.ValueOf(dest)
	//fmt.Println(v.Type().String()) -> *[]main.a

	if v.Kind() != reflect.Ptr {
		log.Println("no pointer")
		return
	}

	ev := reflect.Indirect(v)
	//fmt.Println(v.Type().String()) -> []main.a

	elemIsPtr := ev.Type().Elem().Kind() == reflect.Ptr

	for rows.Next() {
		pnv := reflect.Value{}
		if elemIsPtr {
			pnv = reflect.New(ev.Type().Elem().Elem())
		} else {
			pnv = reflect.New(ev.Type().Elem()).Elem()
		}
		nv := reflect.Indirect(pnv)
		d := make([]interface{}, len(col))
		pd := make([]interface{}, len(col))

		for i, _ := range d {
			pd[i] = &d[i]
		}

		rows.Scan(pd...)
		for i := 0; i < len(col); i++ {
			if f := nv.FieldByName(col[i]); f.IsValid() {
				var str string
				dv := reflect.ValueOf(d[i])

				switch dv.Type().Kind() {
				case reflect.Slice:
					// varchar -> slice 반환
					// sqlite3 driver bug인가??????
					str = string(dv.Bytes())

				default:
					str = fmt.Sprintf("%v", dv.Interface())
				}

				switch f.Type().Kind() {

				case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
					n, _ := strconv.ParseInt(str, 10, 64)
					f.SetInt(n)

				case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
					n, _ := strconv.ParseUint(str, 10, 64)
					f.SetUint(n)

				case reflect.Float32, reflect.Float64:
					n, _ := strconv.ParseFloat(str, 64)
					f.SetFloat(n)

				default:
					f.SetString(str)
				}
			}
		}

		ev.Set(reflect.Append(ev, pnv))
	}

	log.Println(query, " ", va)
}

func Exec(query string, va ...interface{}) (sql.Result, error) {
	log.Println(query, " ", va)
	return goExec(query, va...)
}
