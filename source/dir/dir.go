package dir

import (
	"encoding/json"
	"fmt"

	"gitlab.com/prilus/manga-viewer-go/source/config"
	"gitlab.com/prilus/manga-viewer-go/source/session"
	"gitlab.com/prilus/manga-viewer-go/source/users"
	"gitlab.com/prilus/manga-viewer-go/source/util"
)

type Dir struct{}

func (Dir) List(s *session.Request) {
	path := s.Param("path")
	status := "ok"
	var l interface{}

	defer func() {
		b, _ := json.Marshal(config.APIResult{Result: status, Data: l})
		s.ResponseData(b)
	}()

	if users.GetLoginIdLevel(s) < 2 {
		status = "permission denied"
		return
	}

	l = list(path)
	if l == nil {
		status = "error!"
		return
	}
}

func (Dir) Get(s *session.Request) {
	filename := s.Param("filename")
	filepath := s.Param("filepath")

	if users.GetLoginIdLevel(s) < 2 {
		s.ResponseData(util.HttpCodeImg(403))
		return
	}

	if len(filename) == 0 || len(filepath) == 0 {
		s.ResponseData(util.HttpCodeImg(204))
		return
	}

	b := get(filepath)
	if b == nil {
		s.ResponseData(util.HttpCodeImg(404))
		return
	}

	s.SetHeader("Content-Disposition", fmt.Sprintf("attachment; filename=%s", filename))
	s.ResponseData(b)
}
