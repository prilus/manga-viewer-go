package dir

import (
	"log"
	"os"
	"path/filepath"
	"sort"

	"github.com/facette/natsort"
	"gitlab.com/prilus/manga-viewer-go/source/config"
	"gitlab.com/prilus/manga-viewer-go/source/util"
)

type FileList struct {
	Name string
	Dir  bool
	Size int64
	Time int64
}

func list(path string) []FileList {
	if !util.CheckHackPath(path) {
		return nil
	}

	f, err := os.Open(filepath.Join(config.COMICS_PREFIX, path))
	if err != nil {
		log.Println(err)
		return nil
	}
	defer f.Close()

	list, err := f.Readdir(-1)
	if err != nil {
		return nil
	}

	newList := make([]FileList, 0, len(list))
	for _, v := range list {
		name := v.Name()
		if len(name) <= 0 || name[0] == '.' {
			continue
		}

		isDir := v.IsDir()
		if v.Mode()&os.ModeSymlink != 0 {
			resolvedPath, err := os.Readlink(filepath.Join(config.COMICS_PREFIX, path, v.Name()))
			if err != nil {
				// ignore
				_ = 1
			} else {
				resolvedPathStat, err := os.Stat(resolvedPath)
				if err != nil {
					// ignore
					_ = 1
				} else {
					isDir = resolvedPathStat.IsDir()
				}
			}
		}

		d := FileList{
			Name: name,
			Dir:  isDir,
			Size: v.Size(),
			Time: v.ModTime().Unix(),
		}

		newList = append(newList, d)
	}

	sort.Slice(newList, func(i, j int) bool {
		return natsort.Compare(newList[i].Name, newList[j].Name)
	})

	return newList
}

func get(filename string) []byte {
	if !util.CheckHackPath(filename) {
		return nil
	}

	path := filepath.Join(config.COMICS_PREFIX, filename)
	b, err := os.ReadFile(path)
	if err != nil {
		log.Println(err)
		return nil
	}

	return b
}
