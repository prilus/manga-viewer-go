package session

import (
	"net/http"
	"strconv"

	"gitlab.com/prilus/manga-viewer-go/source/config"
)

type Request struct {
	w             http.ResponseWriter
	r             *http.Request
	formParsed    bool
	sessionLoaded bool
	S             *session
}

func NewRequest(w http.ResponseWriter, r *http.Request) *Request {
	return &Request{
		w: w,
		r: r,
	}
}

func (t *Request) ResponseData(data []byte) {
	t.w.Write(data)
}

func (t *Request) Param(key string) string {
	if t.formParsed == false {
		t.formParsed = true
		t.r.ParseForm()
	}

	return t.r.Form.Get(key)
}

func (t *Request) IntParam(key string) int {
	if n, err := strconv.Atoi(t.Param(key)); err == nil {
		return n
	} else {
		return -1
	}
}

func (t *Request) SetHeader(key string, value string) {
	t.w.Header().Set(key, value)
}

func (t *Request) GetCookie(key string) string {
	if cookie, err := t.r.Cookie(key); err == nil {
		return cookie.Value
	}

	return ""
}

func (t *Request) SetCookie(key string, data string) {
	cookie := http.Cookie{
		Name:  key,
		Value: data,
		//Secure: true, SSL 일때만 쿠키 적용
		HttpOnly: true,
		Path:     "/",
	}

	http.SetCookie(t.w, &cookie)
}

func (t *Request) GetSessionKey() string {
	return t.GetCookie(config.SESSION_COOKIENAME)
}

func (t *Request) LoadSession() {
	if t.sessionLoaded {
		return
	}

	t.sessionLoaded = true
	t.S = LoadSession(t.GetSessionKey())
	t.SetCookie(config.SESSION_COOKIENAME, t.S.Uuid)
}
