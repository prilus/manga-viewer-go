package session

import (
	"encoding/json"
	"log"
	"sync"

	gouuid "github.com/satori/go.uuid"
	"gitlab.com/prilus/manga-viewer-go/source/config"
	"gitlab.com/prilus/manga-viewer-go/source/util"
)

type session struct {
	Uuid      string
	Data      map[string]string
	IpAddress string // 추후 필요하면 사용
}

var sessionMutex sync.RWMutex

func NewSession() *session {
	uuid := gouuid.NewV4()

	return &session{
		Uuid: uuid.String(),
		Data: make(map[string]string),
	}
}

func LoadSession(uuid string) *session {
	if len(uuid) == 0 {
		return NewSession()
	}

	sessionMutex.RLock()

	if b := util.ReadFile(config.SESSION_DIR + uuid); b != nil {
		var s session
		json.Unmarshal(b, &s)
		sessionMutex.RUnlock()
		return &s

	}

	sessionMutex.RUnlock()
	return NewSession()
}

func (t *session) GetData(key string) string {
	return t.Data[key]
}

func (t *session) SetData(key string, data string) {
	if t.Data[key] == data {
		return
	}

	t.Data[key] = data
	t.Write()
}

func (t *session) Write() {
	sessionMutex.Lock()
	defer sessionMutex.Unlock()

	b, err := json.Marshal(t)
	util.WriteFile(config.SESSION_DIR+t.Uuid, b)

	if err != nil {
		log.Println(err)
	}
}
