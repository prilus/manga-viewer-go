package util

import (
	"reflect"
	"log"
	"runtime/debug"
	"strings"
	"crypto/sha256"
	"fmt"
	"crypto/md5"
	"crypto/sha1"
)

func GetNumField(value interface{}) int {
	v := reflect.ValueOf(value)
	v = reflect.Indirect(v)

	if v.Kind() != reflect.Struct {
		log.Println("GetNumField only ptr!!!")
		debug.PrintStack()
		return -1
	}

	return v.NumField()
}

func GetAllField(value interface{}) string {
	v := reflect.ValueOf(value)
	v = reflect.Indirect(v)

	if v.Kind() != reflect.Struct {
		log.Println("GetAllField only ptr!!!")
		debug.PrintStack()
		return ""
	}

	s := make([]string, GetNumField(value))
	for i, _ := range s {
		s[i] = v.Type().Field(i).Name
	}

	return strings.Join(s, ", ")
}

func GetAllQuestionMark(value interface{}) string {
	n := GetNumField(value)
	s := make([]string, n)
	for i, _ := range s {
		s[i] = "?"
	}

	return strings.Join(s, ", ")
}

func Hash(s string) string {
	b := sha256.Sum256([]byte(s))
	return fmt.Sprintf("%064x", b)
}

func HashMD5(s string) string {
	b := md5.Sum([]byte(s))
	return fmt.Sprintf("%032x", b)
}

func HashMysql51(s string) string {
	t := sha1.Sum([]byte(s))
	b := sha1.Sum(t[:])
	return fmt.Sprintf("*%040X", b)
}