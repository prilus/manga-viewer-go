package util

import (
	"fmt"
	"regexp"
	"unicode"
)

const hackPath = "^[./]{1,2}|[.]{1,2}[\\/]"

func CheckHackPath(path string) bool {
	p := regexp.MustCompile(hackPath)
	printable := true

	for _, v := range path {
		if printable = unicode.IsPrint(v); !printable {
			break
		}
	}

	isHackPath := !p.MatchString(path) && printable
	if !isHackPath {
		fmt.Println("hack file ! " + path)
	}

	return isHackPath
}
