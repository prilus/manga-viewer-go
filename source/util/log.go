package util

import (
	"io"
	"log"
	"os"
	"runtime/debug"

	"gitlab.com/prilus/manga-viewer-go/source/config"
)

func NewLogger(name string) *log.Logger {
	f, err := os.OpenFile(config.LOG_DIR+name+".log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		debug.PrintStack()
		panic(err)
	}

	out := io.MultiWriter(os.Stdout, f)

	return log.New(out, name, log.Ldate|log.Ltime|log.Lshortfile)

}
