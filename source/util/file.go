package util

import (
	"fmt"
	"io/ioutil"
	"os"
)

func ReadFile(path string) []byte {
	if f, err := os.Open(path); err == nil {
		defer f.Close()
		if buf, err := ioutil.ReadAll(f); err == nil {
			return buf
		}
	}

	return nil
}

func HttpCodeImg(code int) []byte {
	return ReadFile(fmt.Sprintf("static/img/http_status/%d.jpg", code))
}

func WriteFile(path string, data []byte) error {
	//퍼미션 하드코딩 추후 변경할 것
	return ioutil.WriteFile(path, data, 0644)
}
