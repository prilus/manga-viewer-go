import { writable, derived } from 'svelte/store';

import type { User } from './types';

export const loadingCount = writable(0);
export const isLoading = derived(loadingCount, v => v > 0);

export const user = writable<User | undefined>(undefined);