import { get } from 'svelte/store';

import { user } from './store';
import { APICall } from './apicall';
import type { User } from './types';

export async function loginCheck(): Promise<boolean> {
    const $user = get(user);

    if ($user?.Id) {
        return true;
    }

    try {
        const res = await APICall<User>('/users/info', {});
        user.set(res);
    }
    catch {
        return false;
    }

    return false;
}