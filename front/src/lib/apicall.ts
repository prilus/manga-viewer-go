import { loadingCount } from '@/lib/store';

type APIResult<T> = {
    Result: string
    Data: T
}

export function APIUrl(path: string): string {
    return `/service${path}`;
}

export async function APICall<T>(path: string, data?: unknown): Promise<T> {
    const url = APIUrl(path);

    try {
        loadingCount.update(v => v + 1);

        const reqData = data ? new URLSearchParams(data as Record<string, string>) : undefined;

        const res = await fetch(url, {
            method: data ? 'POST' : 'GET',
            headers: {
                'Content-Type': data ? 'application/x-www-form-urlencoded' : undefined!,
            },
            body: reqData,
        });

        if (res.status !== 200) {
            throw new Error(res.statusText);
        }

        const resBody = await res.json() as APIResult<T>;

        if (!resBody) {
            throw new Error('No response body');
        }

        if (resBody.Result !== 'ok') {
            throw new APIError(resBody.Result);
        }

        return resBody.Data;
    }
    finally {
        loadingCount.update(v => v - 1);
    }
}

export class APIError extends Error {
    constructor(message: string) {
        super(message);
        this.name = 'APIError';
    }
}