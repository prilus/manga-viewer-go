export type User = {
    Id: string
    Level: number
}

export type FileInfo = {
    Name: string;
    Dir: boolean;
    Size: number;
    Time: number;
};