import { defineConfig, loadEnv } from 'vite';
import { svelte } from '@sveltejs/vite-plugin-svelte'
import path from 'node:path';

const env = loadEnv('', process.cwd());

export default defineConfig({
	plugins: [svelte()],
	server: {
		proxy: {
			'/service': {
				target: env.VITE_APP_APISERVER || '',
				changeOrigin: true,
			},
		},
	},
	resolve: {
        alias: {
            '@': path.resolve(__dirname, './src'),
        },
    },
});
