#!/bin/bash

cd `dirname $BASH_SOURCE[0]`

rm -rf static/assets static/index.html

pushd front

npm run build

cp -r dist/* ../static/

popd

go build -o manga-viewer-go .